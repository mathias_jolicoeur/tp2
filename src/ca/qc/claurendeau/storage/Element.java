package ca.qc.claurendeau.storage;

public class Element
{
    private int data;
    private Element next;

    public void setData(int data)
    {
        this.data = data;
    }

    public void setNext(Element next)
    {
    	this.next = next;
    }
    
    public int getData()
    {
        return this.data;
    }
    
    public Element getNext()
    {
    	return this.next;
    }
}
