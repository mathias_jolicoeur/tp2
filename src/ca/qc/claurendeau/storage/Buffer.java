package ca.qc.claurendeau.storage;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer
{
    private int capacity;
    private Element first;
    
    
    public Buffer(int capacity)
    {
        this.capacity = capacity;
    }

    // returns the content of the buffer in form of a string
    public String toString()
    {
    	Element elementTemp = this.first;
    	String string;
    	int nbElement = 1;
    	string = "Element " + nbElement + ": " + elementTemp.getData();
    	while (elementTemp.getNext() != null) {
        	elementTemp = elementTemp.getNext();
        	nbElement ++;
        	string += " Element " + nbElement + ": " + elementTemp.getData();
    	}
        return string;
    }

    // returns the capacity of the buffer
    public int capacity()
    {
        return capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
    	int nbElement = 1;
        Element elementTemp = this.first;
        while (elementTemp.getNext() != null) {
        	elementTemp = elementTemp.getNext();
        	nbElement ++;
        }
        return nbElement;
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
        return first == null;
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
    	return getCurrentLoad() >= capacity();
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) throws BufferFullException
    {
        if (!isEmpty() && !isFull()) {
        	Element elementTemp = this.first;
        	while (elementTemp.getNext() != null)
        		elementTemp = elementTemp.getNext();
        	elementTemp.setNext(element);
        }
        else if (isEmpty()) {
        	this.first = element;
        }
        else {
        	throw new BufferFullException("Buffer Full");
        }
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement()  throws BufferEmptyException
    {
    	if (!isEmpty()) {
	    	Element elementTmp = this.first;
	        this.first= this.first.getNext();
	        return elementTmp;
    	}
    	else {
    		throw new BufferEmptyException("Buffer Empty");
    	}
    }
}
