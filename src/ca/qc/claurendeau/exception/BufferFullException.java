package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {
    
	private static final long serialVersionUID = 1L;

	public BufferFullException(String message) {
		super(message);
	}
	
}
