package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.Test;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class BufferTest {

	static final int NOMBRE_TEST_SET = 3;
	static final int NOMBRE_CAPACITY_TEST = 2;
	static final int NOMBRE_CURRENT_LOAD = 2;
	
	Element element1 = new Element();
	Element element2 = new Element();
	Element element3 = new Element();
	Buffer buffer1;
	Buffer buffer2 = new Buffer(NOMBRE_CAPACITY_TEST);
	
    @Test
    public void testBuildSuccess() {
        assertTrue(true);
    } 
    
    @Test
    public void testSetData() {
    	element1.setData(NOMBRE_TEST_SET);
    	assertTrue(element1.getData() == NOMBRE_TEST_SET);
    }
    
    @Test
    public void testSetElement() {
    	element1.setNext(element2);
    	assertTrue(element1.getNext() == element2);
    }
    
    @Test
    public void testCapacityBuffer() {
    	buffer1 = new Buffer(NOMBRE_CAPACITY_TEST);
    	assertTrue(buffer1.capacity() == NOMBRE_CAPACITY_TEST);
    }
    
    @Test
    public void testAddElement() throws BufferEmptyException, BufferFullException {
    	buffer2.addElement(element1);
    	assertTrue(buffer2.removeElement() == element1);
    }
    
    @Test
    public void testAddMultipleElements() throws BufferFullException, BufferEmptyException {
    	buffer2.addElement(element1);
    	buffer2.addElement(element2);
    	buffer2.removeElement();
    	assertTrue(buffer2.removeElement() == element2);
    }
    
    @Test
    public void testGetCurrentLoad() throws BufferFullException {
    	buffer2.addElement(element1);
    	buffer2.addElement(element2);
    	assertTrue(buffer2.getCurrentLoad() == NOMBRE_CURRENT_LOAD);
    }
    
    @Test
    public void testGetNext() throws BufferFullException {
    	buffer2.addElement(element1);
    	buffer2.addElement(element2);
    	assertTrue(element1.getNext() == element2);
    }
    
    @Test
    public void testBufferIsFullWhenNot() throws BufferFullException {
    	buffer2.addElement(element1);
    	assertTrue(!buffer2.isFull());
    }
    
    @Test
    public void testBufferIsFullWhenItIs() throws BufferFullException {
    	buffer2.addElement(element1);
    	buffer2.addElement(element2);
    	assertTrue(buffer2.isFull());
    }
    
    @Test
    public void testToString() throws BufferFullException {
    	element1.setData(5);
    	element2.setData(24);
    	buffer2.addElement(element1);
    	buffer2.addElement(element2);
    	assertTrue(buffer2.toString().equals("Element 1: 5 Element 2: 24"));
    }
    
    @Test(expected = BufferFullException.class)
    public void testBufferFullException() throws BufferFullException{
    	try {
	    	buffer2.addElement(element1);
			buffer2.addElement(element2);
			buffer2.addElement(element3);
    	} catch (BufferFullException e) {
    		String message = "Buffer Full";
    		assertEquals(message, e.getMessage());
    		throw e;
    	}
    }
    
    @Test(expected = BufferEmptyException.class)
    public void testBufferEmptyException() throws BufferEmptyException {
    	try {
    		buffer2.removeElement();
    	} catch (BufferEmptyException e) {
    		String message = "Buffer Empty";
    		assertEquals(message, e.getMessage());
    		throw e;
    	}
    }
}


